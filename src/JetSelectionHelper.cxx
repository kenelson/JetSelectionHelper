#include "JetSelectionHelper/JetSelectionHelper.h"

// checks the kinematics of the object
bool JetSelectionHelper::isJetGood(const xAOD::Jet* jet) {

  bool passSelect(false);

  if ( jet->pt() > 50000 && fabs(jet->eta()) < 2.5 ){
    passSelect = true;
  }

  return passSelect;
}

// checks if the jet is a b-jet
bool JetSelectionHelper::isJetBFlavor(const xAOD::Jet* jet) {

  bool passSelect(false);

  // btagging selection
  const xAOD::BTagging *btag = jet->btagging();

  double prob_b;
  double prob_c;
  double prob_u;

  btag->pb("DL1r",prob_b);
  btag->pc("DL1r",prob_c);
  btag->pu("DL1r",prob_u);


  if(prob_b>0.8)
    passSelect = true;

  return passSelect;

}
